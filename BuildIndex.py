from xml.dom import minidom #imported minidom parser to read xml file
from Constants import *
import stemmer
import re
import math

class BuildIndex:
    
    '''
        This class takes in a xml dump with a particular structure
        and creates document vectors of all documents
    '''

    def __init__(self, xml_filename):
        self.__xml_filename = xml_filename
        self.__document_vectors = {}
        self.__page_titles = {}
        
    def __get_commonly_used_words(self):
        '''
            This method flags the most commonly used words by reading
            from a file. 
        '''
        f = open(Constants.COMMONLY_USED_WORDS_FILE,'r')
        commonly_used_words = {}

        for word in f.readlines():
            commonly_used_words[word] = 1
        f.close()

        return commonly_used_words

    def __get_document_term_frequencies(self, page, docid, commonly_used_words, stem):
        '''
            This method computes the term frequencies of all the words in a
            given page, including words both in the title and the text of the page,
            but excluding commonly used words.
        '''
        titlelist = page.getElementsByTagName('title')
        title_text = titlelist[0].firstChild.data.lower().strip()
        title_words = re.sub("[^A-Za-z0-9]"," ",title_text).strip().split(' ')
        self.__page_titles[docid] = title_text
        
        textlist = page.getElementsByTagName('text')
        text_text = textlist[0].firstChild.data.lower().strip();
        text_words = re.sub("[^A-Za-z0-9]"," ",text_text).strip().split(' ')

        document_tfs = {}
        all_words = title_words + text_words
        for word in all_words:
            if not commonly_used_words.get(word):
                word = stem.stem(word, 0, len(word) - 1)
                if not len(word) == 0:
                    if not document_tfs.get(word):
                        document_tfs[word] = 0
                    document_tfs[word] = document_tfs[word] + 1
                
        return document_tfs
                                    
    def __compute_document_vectors(self, pagelist):
        '''
            This method casts a set of documents into a vector form. For each
            document, it computes the term frequency-inverse document frequency
            of each word in the document.
        '''
        len_pagelist=len(pagelist)

        stem=stemmer.PorterStemmer()

        commonly_used_words = self.__get_commonly_used_words()

        term_frequencies = {}
        inverse_doc_frequencies = {}
        
        # The loop below gets a list of all the titles and the text from a page
        for doc_num in range(len_pagelist):
            term_frequencies[doc_num] = self.__get_document_term_frequencies(
                pagelist[doc_num], doc_num, commonly_used_words, stem)

            # store the number of pages that a word occurs in
            for word in term_frequencies[doc_num].keys():
                if not inverse_doc_frequencies.get(word):
                    inverse_doc_frequencies[word]=0
                inverse_doc_frequencies[word] += 1

        # compute the tf-idf for each word in each document
        self.__document_vectors = {}
        for doc_num in range(len_pagelist):
            self.__document_vectors[doc_num] = {}
            for word in term_frequencies[doc_num]:
                self.__document_vectors[doc_num][word] = term_frequencies[doc_num][word] * math.log(len_pagelist/inverse_doc_frequencies[word])

    def get_index(self):
        '''
            This method parses an xml file and stores the list of pages in
            the xml file. It finally returns the vector representation of all
            the pages/documents in the xml file.
        '''
        xmldoc = minidom.parse(self.__xml_filename)

        # Pagelist is a list of all pages in the xml file
        pagelist=xmldoc.getElementsByTagName('page')

        self.__compute_document_vectors(pagelist)

        return self.__document_vectors, self.__page_titles

