import numpy
import operator
from cluster import *
from Utils import *
from sklearn import svm

class Similarity:
    '''
        Contains static methods that implement standard algorithms in the field
        of information retrieval and natural language processing, using machine
        learning expedients.
    '''

    @staticmethod
    def get_cosine_score(doc_vec_1, doc_vec_2):
        '''
            This method takes in two document vectors and returns the
            cosine of the angle between them.
        '''
        dot_product = 0.0
        for word in doc_vec_1:
            if doc_vec_2.get(word):
                dot_product += doc_vec_1[word] * doc_vec_2[word]
        magnitude_doc_vec_1 = numpy.linalg.norm(doc_vec_1.values(), ord=2)
        magnitude_doc_vec_2 = numpy.linalg.norm(doc_vec_2.values(), ord=2)
        
        return dot_product/(magnitude_doc_vec_1 * magnitude_doc_vec_2)

    @staticmethod
    def get_top_k_similar_docs(all_doc_vecs, doc_id, k):
        '''
            This method takes in a document and an integer k and it returns
            the top k documents that are similar to it.
        '''
        cosine_score_map = {}
        for doc_num in all_doc_vecs:
            if doc_num != doc_id:
                cosine_score_map[doc_num] = Similarity.get_cosine_score(all_doc_vecs[doc_id], all_doc_vecs[doc_num])

        sorted_x = sorted(cosine_score_map.iteritems(), key=operator.itemgetter(1),reverse=True)

        return sorted_x[:k]

    @staticmethod
    def docs_similar_to_query(all_doc_vecs, doc_vec, k):
        '''
            This method takes as input the vector representation of a query and
            and an integer k and returns the k documents most similar documents
            to the query.
        '''

        cosine_score_map = {}
        for doc_num in all_doc_vecs:
            cosine_score_map[doc_num] = Similarity.get_cosine_score(doc_vec, all_doc_vecs[doc_num])

        sorted_x = sorted(cosine_score_map.iteritems(), key=operator.itemgetter(1),reverse=True)

        return sorted_x[:k]
        

    @staticmethod
    def cluster_docs(all_doc_vecs, k):
        '''
            This method takes in an integer k and clusters the documents passed
            into k clusters using the K-Means++ algorithm.
        '''
        docs_df = DataFrame(all_doc_vecs.values(), index=all_doc_vecs.keys())
        docs_df = docs_df.fillna(0)

        kmpp = KMeansPlusPlus(docs_df, k, appended_column_name='classification')

        kmpp.cluster()

        doc_cluster_map = {}
        for idx,row in docs_df.iterrows():
            doc_cluster_map[idx] = row.values.tolist()[-1]

        return doc_cluster_map

    @staticmethod
    def classify_docs(all_docs_vecs, training_filename, doc_vectors_to_classify):
        '''
            This method takes as input a file with training data and a file with
            testing data. It uses a Multiclass-SVM classifier and learns from
            the training data and predicts a class for each document in the
            test data file. The training data file contains a list of documents
            and their classes and the testing data file contains a list of
            documents.
        '''

        docs_df = DataFrame(all_docs_vecs.values(), index=all_docs_vecs.keys())
        docs_df = docs_df.fillna(0)

        training_data = Utils.get_lines_from_file(training_filename)
        training_data_map = {}

        for training_item in training_data:
            tokens = training_item.split(',')
            training_data_map[int(tokens[0])] = tokens[1]
        
        training_docs_df = docs_df.ix[training_data_map.keys()]
        classifier = svm.SVC()
        classifier.fit(training_docs_df.values,training_data_map.values())

        list_of_docs_to_classify = [int(doc) for doc in Utils.get_lines_from_file(doc_vectors_to_classify)]
        doc_vectors_to_classify_df = docs_df.ix[list_of_docs_to_classify]
        results = classifier.predict(doc_vectors_to_classify_df.values)

        return dict(zip(list_of_docs_to_classify, results.tolist()))
        
