import sys

class Utils:

    '''
        Contains a bunch of utilities that are often used

        Usage:
            map = Utils.get_map_from_csv_file(<filename>)
    '''

    @staticmethod
    def get_map_from_csv_file(filename):
        file_map = {}

        try:
            f = open(filename,'r')
            for line in f:
                line_tokens = line.strip().split(',')
                file_map[line_tokens[0]] = line_tokens[1]
        except Exception as e:
            print "Trouble parsing file:",e
            return {}

        return file_map

    @staticmethod
    def get_lines_from_file(filename):
        try:
            f = open(filename, 'r')
            all_lines = []
            for line in f:
                all_lines.append(line.strip())
            return all_lines
        except Exception as e:
            print "Trouble opening file:",e
            return []
