from BuildIndex import *
from Similarity import *
import sys
from collections import Counter

def print_usage():
    print "Usage: Python main.py <filename> <option> <option parameters>"
    print "<filename> : xml wikidump"
    print "<option> values : 1 2 3 4 5"
    print "<option parameters> : Relevant arguments for option chosen"
    print "For more details, refer README.txt"

def cosine_score(doc_vec_1, doc_vec_2):
    print "The cosine score is:", Similarity.get_cosine_score(doc_vec_1, doc_vec_2)

def top_k_docs(all_doc_vecs, docid, k, page_titles):
    top_k = Similarity.get_top_k_similar_docs(all_doc_vecs, docid, k)
    print "The top",sys.argv[4],"documents are:"
    for item in top_k:
        print item[0], "(" + page_titles[item[0]] + ")"

def cluster_docs(all_doc_vecs, k, page_titles):
    clusters = Similarity.cluster_docs(all_doc_vecs, k)
    print "Document : Cluster"
    for item in clusters:
        print item, "(" + page_titles[item] + ")", ":", int(clusters[item])

def classify_docs(all_doc_vecs, training_filename, testing_filename, page_titles):
    classes = Similarity.classify_docs(all_doc_vecs,training_filename, testing_filename)
    print "Document : Class"
    for item in classes:
        print item, "(" + page_titles[item] + ")", ":", classes[item]

def query(all_doc_vecs, query, k, page_titles):

    query_tf = Counter(query.split())

    top_k = Similarity.docs_similar_to_query(all_doc_vecs, query_tf, k)

    print "The top",k,"documents similar to '" + query +"' are:"
    for item in top_k:
        print item[0], "(" + page_titles[item[0]] + ")"

def main():
    '''
        All of this file is self-explanatory. The file just takes in the
        command line arguments and calls the required module
    '''
    if len(sys.argv) <= 2:
        print_usage()
        sys.exit(0)
        
    filename = sys.argv[1]
    build_index = BuildIndex(filename)
    all_doc_vectors, page_titles = build_index.get_index()

    if sys.argv[2] == '1':
        cosine_score(all_doc_vectors[int(sys.argv[3])], all_doc_vectors[int(sys.argv[4])])

    elif sys.argv[2] == '2':
        top_k_docs(all_doc_vectors, int(sys.argv[3]), int(sys.argv[4]), page_titles)
        
    elif sys.argv[2] == '3':
        cluster_docs(all_doc_vectors, int(sys.argv[3]), page_titles)

    elif sys.argv[2] == '4':
        classify_docs(all_doc_vectors, sys.argv[3], sys.argv[4], page_titles)

    elif sys.argv[2] == '5':
        query(all_doc_vectors, sys.argv[3], int(sys.argv[4]), page_titles)

    else:
        print "Wrong option!"
        print_usage()
        
if __name__ == '__main__':
    main()
