Design Decisions:
	(1) I decided to work off of Wikidumps. The Wikidumps that the program can work with are xml files from - http://dumps.wikimedia.org/enwiki/
	The xml files have the following structure

		<mediawiki>
			<page>
				<title>
				</title>
				<text>
				</text>
			</page>
			<page>
			....
			</page>
		</mediawiki>

	As the name suggests, these files contain a subset of Wikipedia pages. Given the richness of data, I thought it would be a good data source to test these algorithms on.

	(2) This software requires users to give document IDs as input. In particular, for users to use these modules, they would need to know the document IDs of various documents in the wikidump. I have included another file called DocumentIDs_<wikidump name>.txt which contains document IDs for various documents. The file contains the doc id and the title of the document. I chose not to work with titles because it would introduce the extra problem of having to parse input and work with approximate matches.

	(3) This software directly uses two pieces of code obtained on the internet. They are:
		- Stemmer.py - This is an implementation of the Porter-Stemmer algorithm. The algorithm strips words to their roots. For instance, we would want to consider the words 'apples' and 'apple' as the same while trying to compute similarity measures. More details about this are available here - http://tartarus.org/martin/PorterStemmer/
		- cluster.py - This is an implementation of the K-means++ algorithm. This was downloaded from - https://github.com/jackmaney/k-means-plus-plus-pandas

	(4) I decided to ignore the commonly used english words. For instance, words like 'the' must not be allowed to influence document vector orientations too much because they really do not help in disambiguation. A file containing commonly used english words is present in the source directory and this is used by the program.

Features Available:
	I implemented the following modules:
		(a) get_cosine_score(doc1, doc2)
			Cosine score (http://en.wikipedia.org/wiki/Cosine_similarity) is a measure of the angle between two document vectors. It interprets documents as vectors and computes the cosine of the angle between them. This module directly returns the cosine score. Given that I am constructing document vectors using the tf-idf (http://en.wikipedia.org/wiki/Tf%E2%80%93idf) concept, the cosine score is always between 0 and 1, with 0 implying total dissimilarity and 1 implying exact similarity. Thus the cosine score itself can be interpreted directly as a measure of similarity. A user can use this to find the 'similarity strength' between two documents.
			
		(b) get_top_k_documents(all_documents, current_document, k)
			This module finds the cosine similarity between 'current_document' and all other documents in the wikidump and returns the top k most similar documents. A user can use this to find the most similar documents to a particular document.
			
		(c) cluster_docs(all_documents, k)
			This module clusters all documents into k clusters using the K-Means++ algorithm (http://en.wikipedia.org/wiki/K-means%2B%2B). This is useful for grouping similar documents. The k value has to be specified by the user.
		
		(d) classify_docs(all_documents, training_data, testing_data)
			This is a module which allows users to use pre-labelled examples to train a classifier and then use that knowledge to predict the labels of other documents. A user creates a training data file where different documents are assigned to different classes. Then the user also creates a test data file wherein document ids are specified. All this is fed into the module and the module runs 'Multiclass-SVM' to predict the labels of the documents in the test data file.
			
		(e) query(query_string, k)
			This module allows users to specify a string query and obtain the top k documents in the wikidump that might be similar to the document. The user can use this module to the effect of a simplified search engine.
		
Instructions to Execute:
	python Main.py <xml wikidump> <feature option> <arguments required for feature option>
	
	(1) <xml wikidump> - I have provided one small sized xml file in the source package. It can be used directly.

	(2) <feature option> - The possible values are 1, 2, 3, 4 and 5. The values correspond to the module that needs to be invoked. Please refer to the "Features Available" section above. 1 is for (a), 2 is for (b) ..., 5 is for (e)
	
	(3) <arguments required for feature option> - As noted, the features require user input. Here is what you will require for the various features:
		(a) For feature 1, you will need two document ids. An example run is below:
				python Main.py mini-wikidump.xml 1 0 3
			This will give the cosine similarity between documents with doc ID 0 and 3.
		(b) For feature 2, you will need a document id, say doc_id_1 and an integer. An example run is below:
				python Main.py mini-wikidump.xml 2 0 3
			This will give the top 3 most similar documents to the document with document id is 0.
		(c) For feature 3, you will need a single integer specifying the number of clusters. An example run is below:
				python Main.py mini-wikidump.xml 3 4
			This will cluster all documents into four clusters.
		(d) For feature 4, you will need two files, one training csv file and one testing csv file. The training csv file will contain one training item per line, where a training item is a document id followed by a comma, followed by its label. No spaces in between. An example is present in the source directory. The testing csv file will just contain one document id per line. The program will predict the labels of those documents. An example run is below:
				python Main.py mini-wikidump.xml 4 mini-wikidump_sample_training.csv mini-wikidump_sample_testing.csv
			This will train a Multiclass-SVM classifier based on the samples in 'mini-wikidump_sample_training.csv' and then predict the labels of the documents in 'mini-wikidump_sample_testing.csv'
		(e) For feature 5, you will need a query within quotes and an integer. An example run is below:
				python Main.py mini-wikidump.xml 5 "canada election" 3
			This will return the top three documents relevant to the query "canada election"